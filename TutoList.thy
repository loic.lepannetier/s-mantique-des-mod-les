theory TutoList
 
imports Main
 
begin
 
section \<open>Technical stuff to make the tutorial easier\<close>
text \<open>
  Definition of a data type "list of 'a", where 'a' is any type.
 
  We hide the list type of HOL/Main as well as the associated notations
  to avoid conflicts with our definition.
\<close>
  no_notation
        Nil     ("[]")
    and Cons    (infixr "#" 65)
    and append  (infixr "@" 65)
hide_type list
hide_const rev
 
section \<open>Definition of a data type\<close>
text \<open>
  We define a data type named list, which take a type 'a as parameter.
  This data type has two constructors:
  \<^item> Nil, a constant which corresponds to the empty list, and is also noted []
  \<^item> Cons, which adds an item (of type 'a) in front of the list, and is also noted #
  Such a type is an inductive data type because it is defined by induction on
  its constants and constructors.
\<close>
datatype 'a list =
  Nil               ("[]")           (* notation for the empty list *)
| Cons 'a "'a list" (infixr "#" 65)  (* infix right associative notation with priority 65 ( = is 50) *)
 
text \<open>
  Exemples of useful commands to find theorems and print known facts.
 
  To see the result of a command, put the cursor just after the command,
  and look in the "Output" panel. You may have to clock on the "Output" button below to make the panel appear.
 
  Here, we see that "datatype" has generated a lot of theorems about the new type.
  Among them are "list.induct" which allows us to make proofs by structural
  induction on lists, and "list.simps" which gives the simplification rules
  for list terms.
\<close>
find_theorems name:"TutoList.list"
thm TutoList.list.induct
text \<open> Note that A \<Longrightarrow> B \<Longrightarrow> C \<equiv> (A and B) \<Longrightarrow> C, also written \<lbrakk>A; B\<rbrakk> \<Longrightarrow> C \<close>
 
text \<open>Same theorem in a more readable form \<close>
print_statement TutoList.list.induct
 
text \<open>
  The list.induct theorem has been generated automatically from the
  inductive definition of the "list" type.
  It states that for any predicate P, if P holds for the empty list,
  and if for any item x1 and any list x2, when P holds for x2,
  it also holds for x1#x2, then P holds for any list.
\<close>
 
text \<open>Theorems that may be used by the simplifier\<close>
thm TutoList.list.simps
 
section \<open>About Curryfication\<close>
text \<open>
  Curryfication is named after Haskell Curry.
  In Isabelle, functions are in curryfied form:
     f: A \<times> B \<rightarrow> C
        (x, y) \<mapsto> z
   is written as:
     f: A \<rightarrow> (B \<rightarrow> C)
        x \<mapsto> (\<lambda>y. z)
   so "f x" is a function which takes the second argument of f to provide the result,
   and "f x y" is read as "(f x) y"
\<close>
text \<open>Define sum as the function which returns the sum of two integers\<close>
definition sum:: "int \<Rightarrow> int \<Rightarrow> int"
where "sum m n = m + n"
 
text \<open>Check that it works as expected\<close>
value "sum 2 3"
 
text \<open>We can partially apply sum to one argument to define the increment function\<close>
abbreviation "inc \<equiv> sum 1"
 
value "inc 41"
 
 
section \<open>Defining functions\<close>
text \<open>
  To define a function on an inductive data type, it is sufficient
  to define it for each of the constructors.
  For a list, we have to define the function for the empty list and
  for the list obtained by Cons from another list.
\<close>
text \<open>
  Append a list to the end of another list (concatenation).
  append(l1, l2) is written append l1 l2 (Curryfication).
  Thanks to the infix notation, it can also be written l1@l2
\<close>
fun append:: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" (infixr "@" 65)
where
  "[] @ l2 = l2"
| "(x # l1) @ l2 = x # (l1 @ l2)"
 
value "(a # b # c # []) @ (d # e # [])"
text \<open>
  You can see here that since a, b, c, d, and e are free variables, their type
  is not known and is represented by the 'a type variable.
  The list is therefore an 'a list.
  Below, we set the type of the items to nat, so the list become a nat list.
  Note that it is only necessary to set the type of one item because
  all items in a list are of the same type. Here, we have to choose a type
  for 1 because in Isabelle/HOL, 1 may be of any numeral type.
\<close>
value "((1::nat) # 2 # 3 # []) @ (4 # 5 # [])"
 
text \<open>Reverse a list\<close>
fun rev:: "'a list \<Rightarrow> 'a list"
where
  "rev [] = []"
| "rev (x # l) = (rev l) @ (x # [])"
 
text \<open>"value" prints the value of a term.\<close>
value "rev (a # b # c # [])"
 
text \<open>
  Defining a function automatically generates and proves theorems
  that will be used by the simplifier.
\<close>
thm rev.simps(1)
thm rev.simps(2)
 
text \<open>Standard ML code is generated automatically from the definitions.\<close>
ML \<open>
  val frev = @{code rev};    (* the ML function for rev *)
  val fcons = @{code "Cons"};(* the ML function for Cons *)
  val vnil = @{code "Nil"};  (* the ML value for Nil *)
  (* Let's build a list *)
  val l = fcons(1, fcons(2, fcons(3, fcons(4, vnil))));
  (* and reverse it. *)
  frev l;
\<close>
 
section \<open>Prooving facts\<close>
text \<open>
  A lemma stating that appending the empty list to a list does nothing.
  This lemma is proved automatically by induction on the list and then applying the
  simplifier several times. 'simp' applies the simplifier, '+' means as many times
  as necessary to prove all goals.
  The "[simp]" modifier after the name of the lemma says that this lemma can be added
  to the list of facts used by the simp method to simplify terms.
  One should be careful when annotating lemmas with [simp]: the lemma
  should really simplify a term, else the simplifier may enter infinite loops!
 
  \<^emph>\<open>Check the "Proof state" box in the Output panel to see the different steps in the proof of a fact.\<close>
\<close>
lemma append_nil[simp]: "l @ [] = l"
by (induction l, simp+)
 
text \<open>A structured proof of the same lemma\<close>
text \<open>
  Structured proofs are enclosed between "proof" and "qed".
  "proof" can be followed by a method to rewrite the goal. Here, we
  use induction on l. If you do not give a method, Isabelle will
  try to find a suitable one. If you want to work on the raw fact,
  simply put "-" (minus) after "proof". The goal to prove is defined
  as the \<^theory_text>\<open>?thesis\<close> variable. The proof must end with \<^theory_text>\<open>show ?thesis by method\<close>
  (\<^theory_text>\<open>thus\<close> is a shortcut for \<^theory_text>\<open>from this show\<close>).
  Induction on l uses the @{thm list.induct} theorem to split the goal
  in two cases. We can display these cases using the \<^theory_text>\<open>print_cases\<close>
  command. We have cases Nil and Cons. Writing \<^theory_text>\<open>case Nil\<close> puts
  us in the context of the Nil case, where the subgoal to prove
  is defined as the \<^theory_text>\<open>?case\<close> variable. You must write \<^theory_text>\<open>next\<close> to close
  a case before going to the next one.
  Several patterns can be used in such proofs:
  \<^item> \<^theory_text>\<open>from <fact1> have <fact2> by <method>\<close>
    This derives <fact2> from <fact1> using <method>. Method "simp"
    uses facts marked as [simp] to rewrite <fact1>. Method "auto"
    uses simp and tries to solve all pending goals. Instead of
    \<^theory_text>\<open>by <method>\<close>, you can use "." when <fact1> and <fact2> are identical.
  \<^item> \<^theory_text>\<open>have <fact2> using <fact1> by <method>\<close>
    This is identical to \<^theory_text>\<open>from <fact1> have <fact2> by <method>\<close>
  \<^item> The last result is known as "this". When you want to derive
    a fact from this, you can write \<^theory_text>\<open>hence <fact2>\<close> instead of
    \<^theory_text>\<open>from this have <fact2>\<close>. You can also write \<^theory_text>\<open>with <fact1> have <fact2>\<close>
    instead of \<^theory_text>\<open>from <fact1> and this have <fact2>\<close>.
  \<^item> proving a fact by transitivity:
    \<^theory_text>\<open>have "x1 = x2" by <method>
      also have "... = x3" by <method>
      {* more steps here *}
      also have "... = xn" by <method>
      finally have "x1 = xn" by <method>\<close>
  \<^item> accumulating facts:
    \<^theory_text>\<open>have <fact1> by <method>
      moreover have <fact2> by <method>
      {* more steps here *}
      moreover have <factn> by <method>
      ultimately have <conclusion> by <method>\<close>
  You can use the "print_facts" command to display the known facts.
\<close>
lemma "l @ [] = l"
proof (induction l) print_cases
  case Nil thus ?case using append.simps(1) .
next
  case (Cons x l) print_facts thm append.simps(2)
    (* [of v1 v2 ... vn] substitutes terms v1 ... vn to the n first variables of a fact *)
    from append.simps(2)[of "x" "l" "[]"] have "(x # l) @ [] = x # (l @ [])" .
    also with Cons.IH have "... = (x # l)" by simp
    finally show ?case .
qed
 
text \<open>Associativity of append\<close>
text \<open>
  Here, we illustrate another style of proof, where proof methods
  are applied until all goals have been discharged.
  The proof is closed by the "done" keyword.
\<close>
lemma append_assoc: "(l1 @ l2) @ l3 = l1 @ (l2 @ l3)"
  apply (induction l1)
  (* The initial goal has been split into two goals: the base case and the inductive case *)
  apply simp  (* Here, simp gets rid of the base case *)
  apply simp  (* and the last goal is solved again by simp *)
done
 
text \<open>This can be written is a more compact way. "simp+" means "apply simp as much as you can".\<close>
lemma "(l1 @ l2) @ l3 = l1 @ (l2 @ l3)"
by (induction l1, simp+)
 
text \<open>Structured proof of the same lemma\<close>
lemma "(l1 @ l2) @ l3 = l1 @ (l2 @ l3)"
proof (induction l1) print_cases
  case Nil
    from append.simps(1) have "([] @ l2) = l2" .
    hence "([] @ l2) @ l3 = l2 @ l3" by simp
    (* [symmetric] applies symmetry to an equality *)
    also from append.simps(1)[symmetric] have "... = [] @ l2 @ l3" .
    finally show ?case .
next
  case (Cons x l) print_facts
    from append.simps(2) have "((x # l) @ l2) @ l3 = x # (l @ l2) @ l3" by simp
    also from Cons.IH have "... = x # l @ (l2 @ l3)" by simp
    finally show ?case by simp
qed
 
text \<open>Lemma about reversing the concatenation of two lists\<close>
lemma rev_append: "rev (l1 @ l2) = (rev l2) @ (rev l1)"
  apply (induct l1)
  apply simp
  apply auto  (* Here, auto simplifies the goal but does not solve it. *)
  thm append_assoc  (* This theorem about the associativity of '@' will solve the goal. *)
  apply (rule append_assoc)
done
 
text \<open>
  Compact form, where \<^theory_text>\<open>auto simp add <rule>\<close> means "apply auto,
  adding <rule> to the rules the simplifier can use.
\<close>
lemma "rev (l1 @ l2) = (rev l2) @ (rev l1)"
by (induction l1, auto simp add:append_assoc)
 
text \<open>Detailed structured proof of the same lemma\<close>
lemma "rev (l1 @ l2) = (rev l2) @ (rev l1)"
proof (induction l1) print_cases
  case Nil
    from append.simps(1)[of l2] have "rev ([] @ l2) = rev l2" by simp
    also from append_nil[of "(rev l2)", symmetric] have "... = (rev l2) @ []" by simp
    also from rev.simps(1) have "... = (rev l2) @ (rev [])" by simp
    finally show ?case .
next
  case (Cons x l) print_facts
    from append.simps(2)[of x l l2] have "(x # l) @ l2 = x # (l @ l2)" .
    hence "rev ((x # l) @ l2) = rev (x # (l @ l2))" by simp
    also from rev.simps(2)[of x "l @ l2"] have "... = rev (l @ l2) @ x # []" .
    also from Cons.IH have "... = (rev l2 @ rev l) @ x # []" by simp
    also from append_assoc[of "rev l2" "rev l" "x # []"] have "... = rev l2 @ rev l @ x # []" .
    also from rev.simps(2)[of x "[]", symmetric] and rev.simps(1) have "... = rev l2 @ rev (x # l)" by simp
    finally show ?case .
qed
 
text \<open>
  Proof that rev is the inverse of itself, using the previous lemmas
 
  Theorems and lemma are the same from the point of view of Isabelle.
  Lemmas are just theorems of less importance to us.
\<close>
theorem rev_rev: "rev (rev l) = l"
  apply (induct l)
  apply simp
  apply (auto simp add: append_assoc rev_append)
done
 
text \<open>Structured proof of the same theorem\<close>
theorem "rev (rev l) = l"
proof (induction l) print_cases
  case Nil (* show ?case by simp *)
    from rev.simps(1) have "rev Nil = Nil" .
    hence "rev (rev Nil) = Nil" by simp
    thus ?case .
next
  case (Cons x l) print_facts
    have "rev (rev (x # l)) = rev ((rev l) @ (x # []))" by simp
    also have "... = rev (x # []) @ rev (rev l)" using rev_append .
    also from Cons.IH have "... = rev (x # []) @ l" using rev_rev by simp
    also have "... = (rev [] @ (x # [])) @ l" by simp
    also have "... = rev [] @ ((x # []) @ l)" using append_assoc by simp
    also have "... = [] @ (x # []) @ l" by simp
    also have "... = (x # []) @ l" by simp
    also have "... = x # ([] @ l)" by simp
    also have "... = x # l" by simp
    finally show ?case .
qed
 
text \<open>Length of a list\<close>
fun len:: "'a list \<Rightarrow> nat"
where
  "len [] = 0"
| "len (x # l) = Suc (len l)"
 
text \<open>Show that the length of the concatenation of two lists is the sum of their lengths\<close>
theorem append_len: "len (l1 @ l2) = (len l1) + (len l2)"
by (induction l1, simp+)
 
text \<open>Structured proof, not very detailed\<close>
theorem "len (l1 @ l2) = (len l1) + (len l2)"
proof (induction l1) print_cases
  case Nil show ?case by simp
next
  case Cons thus ?case by simp
qed
 
text \<open>The usual "map" operation, which applies a function to the elements of a list\<close>
fun map:: "('a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a list"
where
  "map f [] = []"
| "map f (x # l) = (f x) # (map f l)"
 
text \<open>"abbreviation" defines another name for a term\<close>
abbreviation l4 :: "int list"
where "l4 \<equiv> 1 # 2 # 3 # 4 # []"
 
text \<open>
  Build the list of the squares of l4.
 
  \<lambda>x. x*x is the function which takes an argument and returns its square.
\<close>
value "map (\<lambda>x. x*x) l4"
 
text \<open>Same thing with a function defined explicitly\<close>
definition square:: "int \<Rightarrow> int"
where "square n = n * n"
 
value "map square l4"
 
end










