theory NikOpNaiveExercise

imports NikInstructions
 
begin
section \<open>Naïve attempt at an operational semantics\<close>
 
text \<open>
  Naïve attempt to define the semantics of the instructions
  using a recursive function.
\<close>
fun execute:: "instruction \<Rightarrow> state \<Rightarrow> state"
where
    "execute Nop s = s"
  | "execute (var v) s = s(v:=0)" 
  \<comment> \<open>Other cases...\<close>
  | "execute (i1 ;; i2) s = (
      let s1 = execute i1 s in
        execute i2 s1
      )"

  | "execute (if c then i1 else i2 fi) s = (
      if (evabool c s) then
        execute i1 s
      else
        execute i2 s )"

  | "execute (while c do body done) s = (
      if (evalbool c s) then
        let s' = execute body s in
         execute (while c do body done) s'
      else
        s
     )"



end