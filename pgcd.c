// Ajoutez ici les prédicats et lemmes nécessaires
//@ predicate is_factor(int a, int n) = (exists b. b*a == n);
// prédicat de test si n est bien le pgcd de n1 et n2. Vrai si n est un facteur commun et qu'il n'existe pas de facteur plus grand
//@ predicate pgcd_bool(int n1, int n2, int n) = is_factor(n, n1) && is_factor(n, n2) &&  !(exists k. is_factor(k, n1) && is_factor(k, n2) && k > n);

//@ lemma pgcd_sym : forall a,b,n. pgcd_bool(a,b,n) <-> pgcd_bool(b,a,n);
//@ lemma pgcd_id : forall a. pgcd_bool(a,a,a) <-> pgcd_bool(a,a,a);

int pgcd(int a, int b) {
  //@requires a > 0;
  //@requires b > 0;
  //@ensures pgcd_bool(old(a), old(b), result);
	
	
  // Le résultat doit être le PGCD de a et b
  while (a != b) {
  	//@ variant a + b;
  	//@ invariant a>0;
  	//@ invariant b>0;

    if (a < b) {
      b -= a;
    } else {
      a -= b;
    }
  }
  return a;
}
