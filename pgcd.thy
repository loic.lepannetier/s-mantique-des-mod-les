theory pgcd
  imports Main
begin
	definition \<open>is_factor (a::int) (n::int) \<equiv>
	  (\<exists>b. (b * a) = n)
	\<close>

	definition \<open>pgcd_bool (n1::int) (n2::int) (n::int) \<equiv>
	  (is_factor n n1) \<and> ((is_factor n n2) \<and> (\<not> (\<exists>k. (is_factor k n1) \<and> ((is_factor k n2) \<and> (k > n)))))
	\<close>

lemma pgcd_sym: \<open>(pgcd_bool a b n) = (pgcd_bool b a n)\<close> 
  unfolding pgcd_bool_def by auto

lemma pgcd_id: \<open>(pgcd_bool a a a) = (pgcd_bool a a a)\<close> 
  unfolding pgcd_bool_def by auto

	(*
	  int pgcd (int a, int b) {
	    //@ requires a > 0 ;
	    //@ requires b > 0 ;
	    //@ ensures pgcd_bool old(a) old(b) result ;
	    while (a != b) {
	      //@ variant a + b;
	      //@ invariant a > 0;
	      //@ invariant b > 0;
	      if (a < b) {
	          b -= a;
	      } else {
	          a -= b;
	      }
	    }
	    return a;
	  }
	*)

end
