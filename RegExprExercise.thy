theory RegExprExercise

imports Main

begin

text \<open>
  A theory of simple regular expressions on an alphabet 'a.
  We handle:
  \<^item> the empty regular expression \<epsilon>, which matches nothing
  \<^item> the atomic regular expression, which matches its atom
  \<^item> the alternative between two regular expressions, which matches either one or the other
  \<^item> the sequence of regular expressions, which matches a sequence of matches of two REs
  \<^item> the Kleene closure of a regular expression, which matches any sequence of matches of the RE
\<close>
datatype 'a regexpr =
    Empty                           (\<open>\<epsilon>\<close>)
  | Atom 'a                         (\<open><_>\<close>)
  | Alt \<open>'a regexpr\<close> \<open>'a regexpr\<close>   (infixr \<open>.|.\<close> 65)
  | Seq \<open>'a regexpr\<close> \<open>'a regexpr\<close>   (infixr \<open>.+.\<close> 75)
  | Star \<open>'a regexpr\<close>               (\<open>_*\<close>)

text \<open>
  Some examples on characters
\<close>
term \<open><CHR ''a''>\<close>
term \<open><CHR ''a''> .|. <CHR ''b''>\<close>
term \<open><CHR ''a''>*\<close>
term \<open>(<CHR ''a''> .|. <CHR ''b''>)*\<close>
term \<open>(<CHR ''a''> .|. <CHR ''b''>)* .+. <CHR ''c''>\<close>

text \<open>
  Languages are sets of words, a word being an 'a list.
\<close>
type_synonym 'a language = \<open>'a list set\<close>

text \<open>
  The Kleene closure of a language is the smallest set that contains:
  \<^item> the empty word
  \<^item> all concatenations of words of the language.

  inductive_set is like inductive, but directly defines a set
\<close>
inductive_set Kleene :: \<open>'a language \<Rightarrow> 'a language\<close> (\<open>_\<star>\<close>)
                      for L :: \<open>'a language\<close>
where
    \<comment> \<open>The empty word belongs to the Kleene closure\<close>
    Empty:  \<open>[] \<in> Kleene L\<close>
    \<comment> \<open>Which other words belong to the closure? Think about 'append', the @ operator on lists\<close>
  | Cons:   \<open>\<lbrakk>w \<in> L; w' \<in> Kleene L\<rbrakk> \<Longrightarrow> w@w' \<in> Kleene L\<close>

text \<open>
  Language recognized by a regular expression
\<close>
fun language :: \<open>'a regexpr \<Rightarrow> 'a language\<close>
where
    Lang_Empty: \<open>language \<epsilon> = {}\<close>
  | Lang_Atom:  \<open>language <a> = {[a]}\<close>
  | Lang_Alt:   \<open>language (r1 .|. r2) = (language r1) \<union> (language r2)\<close>
  | Lang_Seq:   \<open>language (r1 .+. r2) = {w1@w2 | w1 w2 . (w1 \<in> language r1) \<and> (w2 \<in> language r2)}\<close>
  | Lang_Star:  \<open>language (r*) = ((language r)\<star>)\<close>


section \<open>Some properties\<close>

text \<open>What is the language of the empty regexpr?\<close>
lemma epsilon_empty: \<open>language \<epsilon> = {}\<close>
  by simp

lemma \<open>[]@[] = []\<close> by simp

text \<open>And what about its Kleene closure?\<close>
lemma \<open>language (\<epsilon>*) = {[]}\<close>
  using Kleene.simps by auto

text \<open>The alternative commutes\<close>
lemma \<open>language (A .|. B) = language(B .|. A)\<close>
  by auto

text \<open>\<epsilon> is left and right absorbant for the sequence\<close>
lemma \<open>language(\<epsilon> .+. A) = language \<epsilon>\<close> by auto
lemma \<open>language(A .+. \<epsilon>) = language \<epsilon>\<close> by auto

text \<open>\<epsilon> is neutral for the alternative\<close>
lemma \<open>language (\<epsilon> .|. A) = language A\<close> by auto
lemma \<open>language (A .|. \<epsilon>) = language A\<close> by auto

text \<open>The language of a regexpr is included in its Kleene closure\<close>
lemma kleene_includes: \<open>language r \<subseteq> Kleene (language r)\<close>
proof
  {fix x assume \<open>x \<in> language r\<close>
    from Kleene.Cons[OF this, OF Kleene.Empty] have \<open>x@[] \<in> Kleene (language r)\<close> .
    then show \<open>x \<in> Kleene (language r)\<close> using append_Nil2 by simp
  }
qed

text \<open>Correspondance between Kleene and Star\<close>
lemma morph_kleene: \<open>language (r*) = ((language r)\<star>)\<close>
  by simp

\<comment> \<open> Correction prof : \<close>
text \<open>The Kleene closure is stable by concatenation\<close>
lemma kleene_closed: \<open>\<lbrakk>w \<in> (L\<star>); w' \<in> (L\<star>)\<rbrakk> \<Longrightarrow> w@w' \<in> (L\<star>)\<close>
proof (induction w rule: Kleene.induct)
  case Empty
  then show ?case by simp
next
  case (Cons w1 w2)
  then show ?case by (simp add: Kleene.Cons)
qed

text \<open>Dual version for regexpr\<close>
corollary kleene_closed': \<open>\<lbrakk>w \<in> language (r*); w' \<in> language (r*)\<rbrakk> \<Longrightarrow> w@w' \<in> language (r*)\<close>
  by (simp add: kleene_closed)

text \<open>
  Some checks on concrete strings and regexprs.
\<close>
schematic_goal a_or_b_then_c_star:
  \<open>language (((<CHR ''a''> .|. <CHR ''b''>) .+. <CHR ''c''>)*) = ?X\<close>
by simp

thm a_or_b_then_c_star

lemma \<open>''acbc'' \<in> language (((<CHR ''a''> .|. <CHR ''b''>) .+. <CHR ''c''>)*)\<close> (is \<open>?w \<in> language (?R*)\<close>)
proof -
  have \<open>''ac'' \<in> language (?R)\<close> by simp
  hence 1:\<open>''ac'' \<in> language (?R*)\<close> using kleene_includes by blast
  have \<open>''bc'' \<in> language (?R)\<close> by simp
  hence 2:\<open>''bc'' \<in> language (?R*)\<close> using kleene_includes by blast
  have \<open>?w = ''ac''@''bc''\<close> by simp
  with kleene_closed'[OF 1 2] show ?thesis by presburger
qed

end

