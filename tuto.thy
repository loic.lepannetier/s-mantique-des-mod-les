theory tuto

imports tutoList

begin

text  \<open>Exercice 1\<close>
fun snoc:: "'a list \<Rightarrow> 'a \<Rightarrow> 'a list"
  where "snoc [] x = x # []"
  | "snoc (y # l) x = x # (snoc l y)"



fun  rev_snoc:: "'a list \<Rightarrow> 'a list"
where
    "rev_snoc [] = []"
  | "rev_snoc (x # l) = snoc (rev_snoc l) x"

end