theory NikDenNaive

imports NikInstructions

begin

text \<open> The denotation of an instruction is a relation between states. \<close> 

type_synonym instr_den = "state rel" \<comment> \<open>same as "(state \<times> state) set"\<close>

fun nik_den:: "instruction \<Rightarrow> instr_den" ("\<lbrakk> _ \<rbrakk>")
where
  Nop_den:    "\<lbrakk>Nop\<rbrakk> = Id"
| Var_den:    "\<lbrakk>var v\<rbrakk> = {(s, s'). s' = s(v:=0)}"
| Assign_den: "\<lbrakk>v<-val\<rbrakk> = {(s, s'). s' = s(v:=evaluate val s)}"

\<comment> \<open>Hint for the next one: 
  O is the composition of relations (like o is the composition of functions):
  \<lbrakk> (a, b) \<in> r ; (b, c) \<in> s \<rbrakk> \<Longrightarrow> (a, c) \<in> r O s\<close>
| Seq_den:    "\<lbrakk>i1;; i2\<rbrakk> = \<lbrakk>i1\<rbrakk> O \<lbrakk>i2\<rbrakk>"
\<comment> \<open>Interet de cette méthode montrer que if and else de isabelle c'est le même\<close>
| Alt_den:    "\<lbrakk>if c then iT else iF fi\<rbrakk> = 
                 { (s,s').
                    if evalbool c s then
                      (s,s') \<in> \<lbrakk>iT\<rbrakk>
                    else 
                       (s,s')\<in> \<lbrakk>iF\<rbrakk>
                    }"
| Alt_den2:    "\<lbrakk>if c then iT else iF fi\<rbrakk> = 
                 { (s, s'). evalbool c s \<and> (s, s') \<in> \<lbrakk>iT\<rbrakk>}
                 \<union>
                 { (s, s'). \<not>evalbool c s \<and> (s, s') \<in> \<lbrakk>iT\<rbrakk>}"
| Alt_den3:    "\<lbrakk>if c then iT else iF fi\<rbrakk> = 
                 { (s, s'). 
                    evalbool c s \<and> (s, s') \<in> \<lbrakk>iT\<rbrakk>}
                 \<union>
                    \<not>evalbool c s \<and> (s, s') \<in> \<lbrakk>iT\<rbrakk>}"

| While_den:  "\<lbrakk>while c do body done\<rbrakk> = {
                (s,s').
                  if evalbool c s then 
                    (s, s') \<in> \<lbrakk>body;; while c do body done\<rbrakk>
                  else 
                    (s, s') \<in> Id
                  }"

end