theory NikBigStepExercise
imports NikInstructions

begin
section \<open>Big step operative semantics of Niklaus\<close>
 
text \<open>
  In big step semantics, we define a relation between a pair (instruction, state)
  and the state reached after executing the instruction from the original state.
  (i, s) and s' are in relation if by executing instruction i in state s we reach
  state s'.
 
  Defining a relation r amounts to defining a predicate that takes two arguments and returns:
  \<^item> True is the arguments are in relation
  \<^item> False is they are not
 
  Here, we use an inductive definition, which tells which pairs of (instruction, state)
  and state are in relation. We cannot use a function (see theory NikInstructions) 
  because we cannot prove its termination.
  The infix notation (i, s) \<leadsto> s', which states that (i, s) and s' are in relation,
  can be read as "executing i in state s leads to state s'".
\<close>
inductive big_step :: "(instruction \<times> state) \<Rightarrow> state \<Rightarrow> bool" (infix "\<leadsto>" 55)
where
    Nop: "(Nop, s) \<leadsto> s" 
  | VDecl: "(var v, s) \<leadsto> s(v:=0)"
  | Assign: "(v <- e, s) \<leadsto> s(v:=evaluate e s)"
  | Seq: "\<lbrakk>(i1, s) \<leadsto> s'; (i2, s') \<leadsto> s''\<rbrakk>
          \<Longrightarrow> (i1;; i2, s) \<leadsto> s''"
  | IfTrue:     "\<lbrakk>evabool c s ; (i1, s) \<leadsto> s' \<rbrakk> \<Longrightarrow> (if c then i1 else i2 fi, s) \<leadsto> s'"
  | IfFalse:    "\<lbrakk>\<not>evabool c s ; (i2, s) \<leadsto> s' \<rbrakk> \<Longrightarrow> (if c then i1 else i2 fi, s) \<leadsto> s'"
  | WhileFalse: "\<not>evabool c s \<Longrightarrow> (while c do body done, s) \<leadsto> s"
  | WhileTrue:  "\<lbrakk>evabool c s; (body, s) \<leadsto> s'; (while c do body done, s') \<leadsto> s''\<rbrakk>
                  \<Longrightarrow> (while c do body done, s) \<leadsto> s''"

print_theorems
 
text \<open>Some proofs to check that this works properly\<close>
 
text \<open>P[of t1 t2 ... tn] substitutes t1, t2, ... tn to the n first schematic variables of P\<close>
thm VDecl
thm VDecl[of "''x''" "s"]
thm VDecl[where v="''x''" and s="s"]
 
lemma "(var ''x'', s) \<leadsto> s(''x'' := 0)"
by (rule VDecl[of "''x''" "s"])
 
text \<open>The same lemma, but we let the unifier find substitutions\<close>
lemma "(var ''x'', s) \<leadsto> s(''x'' := 0)"
by (rule VDecl)
 
text \<open>We can even let Isabelle find the rule by itself...\<close>
lemma "(var ''x'', s) \<leadsto> s(''x'' := 0)" ..
 
text \<open>An auxilliary lemma to help with the assignment of constants\<close>
lemma AssignConst: \<open>(v <- N n, s) \<leadsto> s(v := n)\<close>
proof -
  have "evaluate (N n) s = n" by simp
  with Assign[where e=\<open>N n\<close>] show ?thesis by simp
qed


lemma "(''x'' <- N 3, s) \<leadsto> s(''x'' := 3)"
\<comment> \<open>Here, we cannot apply the Assign rule directly, because this rule expects
    a term in the form "evaluate e pre". We use AssignConst instead:\<close>
  apply (rule AssignConst)
done
 
text \<open>
  In a schematic goal, schematic variables will be determined during the proof.
  Here, we will find the final state ?s'
\<close>
schematic_goal ex1:  \<comment> \<open>use schematic_lemma in versions of Isabelle before 2016\<close>
  "(''x'' <- N 3 ;; ''y'' <- V ''x'', s) \<leadsto> ?s'"
\<comment> \<open>Applying the Seq rule goes backward to the premisses of the lemma\<close>
apply (rule Seq)
\<comment> \<open>The Assign rule discharges the first premisse\<close>
apply (rule Assign)
\<comment> \<open>simp simplifies evaluate (N 3) s\<close>
apply (simp)
\<comment> \<open>The Assign rule discharges the second premisse\<close>
apply (rule Assign)
done
text \<open>
  ?s' was determined during this proof, we can get its value in the simplified form of the lemma
\<close>
thm ex1
thm ex1[simplified]
 
text \<open>Computing the GCD of 2 and 3 by successive subtractions\<close>
schematic_goal gcd2_3:
"   (''a''<- N 2;; ''b''<- N 3;;
    while V ''a'' .\<noteq>. V ''b'' do
      if V ''a'' .<. V ''b'' then
        ''b''<- V ''b'' .-.V ''a''
      else
        ''a''<- V ''a'' .-. V ''b''
      fi
    done,
    \<lambda>x.0) \<leadsto> ?s
"
  apply (rule Seq)
  apply (rule AssignConst)
  apply (rule Seq)
  apply (rule AssignConst)
  apply (rule WhileTrue)
  apply simp
  apply (rule IfTrue)
  apply simp
  apply (rule Assign)
  apply simp
  apply (rule WhileTrue)
  apply simp
  apply (rule IfFalse)
  apply simp
  apply (rule Assign)
  apply simp
  apply (rule WhileFalse)
  apply simp
  done
thm gcd2_3
 
text \<open>To avoid to much copy-pasting, we define an abbreviation\<close>
abbreviation "GCD_2_3 \<equiv> 
''a'' <- N 2 ;; ''b'' <- N 3 ;;
    while V ''a'' .\<noteq>. V ''b'' do
      if V ''a'' .<. V ''b'' then
        ''b'' <- V ''b'' .-. V ''a''
      else
        ''a'' <- V ''a'' .-. V ''b''
      fi
    done
"
 
text \<open>Generate code for predicate big_step\<close>
code_pred big_step .
 
text \<open>Compute the result of a program using this generated code\<close>
values "{res. (GCD_2_3, \<lambda>x.0) \<leadsto> res }"
 
text \<open>Use map to display the value of a and b in the environment of the result\<close>
values "{map res [''a'', ''b'']|res. (GCD_2_3, \<lambda>x.0) \<leadsto> res }"
 
 
end