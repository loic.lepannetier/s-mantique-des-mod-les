theory NikExprSimpExercise

imports NikExpressions

begin
section \<open>Simplification of Niklaus arithmetic expressions\<close>

\<comment> \<open>This is a template to be completed by the students\<close>

text \<open>
  Simplifying an expression amounts to replacing:
  \<^item> all constant expressions by their value (e.g. N 2 .+. N 3 by N 5)
  \<^item> N 0 .+. x and x .+. N 0 by x
  \<^item> N 0 .*. x and x .*. N 0 by N 0
  \<^item> N 1 .*. x and x .*. N 1 by x
  \<^item> N 0 .\<div>. x by N 0
  \<^item> x .\<div>. N 1 by x
\<close>
fun simplify:: "expression \<Rightarrow> expression"
where
    \<comment> \<open>A binary operator can be simplified if both arguments simplify to numbers\<close>
    "simplify (a .+. b) = (
       let (u', v') = (simplify a, simplify b) in
       if u' = (N 0) then v'
       else if v' = (N 0) then u'
       else
       case (u', v') of
           (N va, N vb) \<Rightarrow> N (va + vb)  \<comment> \<open>arguments are numbers \<Rightarrow> compute the result\<close>
         | (sa, sb) \<Rightarrow> sa .+. sb        \<comment> \<open>else return the sum of the simplified arguments\<close>
    )"
  | "simplify (a .-. b) = (
      let (u', v') = (simplify a, simplify b) in
      if v' = (N 0) then u'
      \<comment> \<open>else if u' = (N 0) then -v'\<close>
      else
      case (u', v') of
           (N va, N vb) \<Rightarrow> N (va + vb)  \<comment> \<open>arguments are numbers \<Rightarrow> compute the result\<close>
         | (sa, sb) \<Rightarrow> sa .+. sb        \<comment> \<open>else return the sum of the simplified arguments\<close>
    )"
  | "simplify (a .*. b) = (
      let (u', v') = (simplify a, simplify b) in
      if u' = (N 0) then N 0
      else if v' = (N 0) then N 0
      else if u' = (N 1) then v'
      else if v' = (N 1) then u'
      else
      case (u', v') of
           (N va, N vb) \<Rightarrow> N (va * vb)  \<comment> \<open>arguments are numbers \<Rightarrow> compute the result\<close>
         | (sa, sb) \<Rightarrow> sa .*. sb        \<comment> \<open>else return the sum of the simplified arguments\<close>
    )"
  | "simplify (a .\<div>. b) = (
      let (u', v') = (simplify a, simplify b) in
      if u' = (N 0) then N 0
      \<comment> \<open>else if v' = (N 0) then N 0 // remplacer par un retour d'erreur ? ?\<close>
      else if v' = (N 1) then u' \<comment> \<open>cas u = 1 pas pertinent\<close>
      else
      case (u', v') of
           (N va, N vb) \<Rightarrow> N (va div vb)  \<comment> \<open>arguments are numbers \<Rightarrow> compute the result\<close>
         | (sa, sb) \<Rightarrow> sa .\<div>. sb        \<comment> \<open>else return the sum of the simplified arguments\<close>
    )"
  \<comment> \<open>Default: variables and constants cannot be simplified and are left as is\<close>
  | "simplify expr = expr"

text \<open>Some checks\<close>
value "simplify ((N 2) .*. (N 3))"
value "simplify (N 2 .*. N 1 .+. V ''x'')"
value "simplify (N 0 .*. (N 1 .+. V ''x''))"
value "simplify ((N 2 .-. N 2) .*. (N 1 .+. V ''x''))"

text \<open>Proof that simplify does not change the semantics of expressions\<close>
theorem "evaluate (simplify expr) env = evaluate expr env"
\<comment> \<open>
  Hint: there is a theorem @{thm expression.split} that splits a proof on expressions
  into smaller proofs on the different kinds of expressions.
  You can use it with simp, the syntax is "apply (simp split: expression.split)".
  This also works with "auto".
\<close>
(* Preuve d'une partie du theorem mais je n'arrive pas à résoudre les 4 derniers subgoals
   ni mettre un 'sorry' apres ma preuve afin qu'il skip ces 4 derniers subgoals.
 apply (induction expr)
 apply (simp split : expression.split)
proof (induction expr)
  case (Value x)
  then show ?case by simp
next
  case (Variable x)
  then show ?case by auto
next
  case (Sum expr1 expr2)
  then show ?case by auto
next
  case (Diff expr1 expr2)
  then show ?case by auto
next
  case (Prod expr1 expr2)
  then show ?case by auto
next
  case (Quot expr1 expr2)
  then show ?case by auto
next*)
sorry

text \<open>
  A predicate telling whether an expression is optimal, i.e. does not
  contain any operator applied to constants.
\<close>
fun optimal_expr :: "expression \<Rightarrow> bool"
where
    "optimal_expr (N a) = True"
  | "optimal_expr (V x) = True"
  | "optimal_expr (a .+. b) = (
     case (a, b) of
           (N va, N vb) \<Rightarrow> False  \<comment> \<open>arguments are numbers \<Rightarrow> not optimal\<close>
           | (expr_a, expr_b) => True)"
  | "optimal_expr (a .-. b) = (
      case (a, b) of
           (N va, N vb) \<Rightarrow> False  \<comment> \<open>arguments are numbers \<Rightarrow> not optimal\<close>
           | (expr_a, expr_b) => True)"
  | "optimal_expr (a .*. b) = (
     case (a, b) of
           (N va, N vb) \<Rightarrow> False  \<comment> \<open>arguments are numbers \<Rightarrow> not optimal\<close>
           | (expr_a, expr_b) => True)"
  | "optimal_expr (a .\<div>. b) = (
     case (a, b) of
           (N va, N vb) \<Rightarrow> False  \<comment> \<open>arguments are numbers \<Rightarrow> not optimal\<close>
           | (expr_a, expr_b) => True)"



text \<open>Check that optimal_expr behaves as expected\<close>
value "optimal_expr (N 3)"
value "optimal_expr (N 3 .+. V ''x'')"
value "optimal_expr (N 0 .+. V ''x'')"
value "optimal_expr (N 3 .+. N 5)"
value "optimal_expr ((N 3 .+. V ''x'') .*. V ''y'')"
value "optimal_expr ((N 3 .+. N 2) .*. V ''y'')" \<comment> \<open> ERREUR : doit générer False normalement pourtant le cas cst + cst est correctement traité selon le test 4 \<close>
value "optimal_expr (N 0 .*. V ''x'')"

text \<open>Prove that simplify yields optimal expressions\<close>
theorem "optimal_expr (simplify expr)"
\<comment> \<open>
  Hint: since the simplification of an operator depends on the form of
  the simplification of its arguments, we have to analyse the different
  cases. This can be done using the "case_tac" tactic.
  For instance, to analyse all the cases for expression e, we
  can use "apply (case_tac \<open>e\<close>)", and then process each case.
\<close>
 
  apply (induction expr)
 (*apply auto // bloque la preuve par auto dans les cas*) 
proof (induction expr)
  case (Value x)
  then show ?case by auto
next
  case (Variable x)
  then show ?case by auto
next
  case (Sum expr1 expr2)
  then show ?case by auto
next
  case (Diff expr1 expr2)
  then show ?case by auto
next
  case (Prod expr1 expr2)
  then show ?case by auto
next
  case (Quot expr1 expr2)
  then show ?case by auto
sorry
(* Pareil que la preuve du théorem precedent, je suis bloqué sur les derniers subgoals.*)

end
