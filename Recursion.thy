theory Recursion
imports Main
begin
 
text \<open>Define the factorial\<close>
fun f :: "nat => nat"
where
   "f 0 = 1"
 | "f (Suc n) = (Suc n) * f n"
 
text \<open>Some checks\<close>
value "f 0"
value "f 1"
value "f 5"
 
 
text \<open>Define the functor whose fixed point is the factorial\<close>
definition Fact :: "(nat => nat) => (nat => nat)"
where
   "Fact u \<equiv> (\<lambda>n. (case n of 0 \<Rightarrow> 1 | Suc n' \<Rightarrow> n*(u n')))"
 
text \<open>
  Check what happens with successive applications 
  of the functor to the null function.
  ^^ is the composition power: f^^n = f \<circ> f \<circ> ... \<circ> f n times
\<close>
value "Fact (\<lambda> x. 0)"
value "(Fact (\<lambda> x. 0)) 0"
value "(Fact (Fact (\<lambda> x. 0))) 1"
value "((Fact^^2) (\<lambda> x. 0)) 1"
value "((Fact^^3) (\<lambda> x. 0)) 2"
value "((Fact^^4) (\<lambda> x. 0)) 3"
value "((Fact^^5) (\<lambda> x. 0)) 4"
value "((Fact^^6) (\<lambda> x. 0)) 5"
 
text \<open>Show that f is a fixed point of Fact\<close>
lemma "Fact f = f"
  by (unfold Fact_def, rule ext, simp split: nat.split)
 
text \<open>Non constructive definition of the factorial\<close>
function g ::"nat \<Rightarrow> nat"
where
  "g 0 = 1"
| "g (Suc n) = (g (Suc (Suc n))) div (Suc n)"
  using not0_implies_Suc apply fastforce apply simp apply blast by simp
termination sorry
 
text \<open>Define the matching functor\<close>
definition G :: "(nat => nat) => (nat => nat)"
where
   "G u \<equiv> (\<lambda>n. (case n of 0 \<Rightarrow> 1 | Suc n' \<Rightarrow> (u n) div n))"
 
 
text \<open>
  Check what happens with successive applications 
  of the functor to the null function.
\<close>
value "(G (\<lambda>n. 0)) 0"
value "((G^^10) (\<lambda>n. 0)) 1" (* Cannot build g *)
 
text \<open>
  The factorial f is also a fixed point of G,
  but we cannot use G^^i to approximate it.
\<close>
lemma "G f = f"
proof (unfold G_def, rule ext)
  fix n
  show \<open>(case n of 0 \<Rightarrow> 1 | Suc n' \<Rightarrow> f n div n) = f n\<close>
  proof (cases n)
    case 0
    then show ?thesis by simp
  next
    case (Suc n')
    have \<open>f (Suc n) div (Suc n) = (Suc n) * (f n) div (Suc n)\<close> by simp
    also have \<open>... = f n\<close>
      using nonzero_mult_div_cancel_left by blast
    then show ?thesis using Suc by simp
  qed
qed

text \<open>Conjecture de Syracuse\<close>
function h :: "nat \<Rightarrow> nat"
where
  "h n = ( if n=1 then 1 else if n mod 2 = 0 then h(n div 2) else h (Suc (3*n)))" 
  by simp+
termination sorry


text \<open>Some checks...\<close>
value "h 0"
value "h 2"
value "h (1+1)"
 
text \<open>Definition of the matching functor\<close>
definition Syr :: "(nat => nat) => (nat => nat)"
where
  "Syr u \<equiv> (\<lambda>n. if n<2 then 1
                else if n mod 2 = 0 then u (n div 2)
                else u (Suc (3*n)))"
 
text \<open>Find the right power P to be able to terminate each call\<close>
value "Syr (\<lambda>x. 0) 0"
value "Syr (\<lambda>x. 0) 1"
value "(Syr^^2) (\<lambda>x. 0) 2"
value "(Syr^^8) (\<lambda>x. 0) 3"
value "(Syr^^3) (\<lambda>x. 0) 4" 
value "(Syr^^6) (\<lambda>x. 0) 5"
value "(Syr^^9) (\<lambda>x. 0) 6" (* 1 + cas avec 3*)
 
text \<open>Show that \<lambda>n. 1 a fixed point of Syr\<close>
lemma "Syr (\<lambda>n. 1) = (\<lambda>n. 1)"
  by (unfold Syr_def, rule ext, simp split: nat.split)
 
end