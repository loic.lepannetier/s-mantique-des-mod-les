// Lemmes nécessaires pour l'arithmétique non linéaire
//@ lemma distr_right: forall x, y, z. x*(y+z) == (x*y)+(x*z) ;
//@ lemma distr_left: forall x, y, z. (x+y)*z == (x*z)+(y*z) ;


int isqrt(int x) {
  // Le résultat doit être la partie entière de la racine carrée de x
  //@ requires x > 0;
  //@ ensures result*result <= x && x <= (result+1)*(result+1);
  
  int count = 0;
  int sum = 1;
  while (sum <= x) { 
  	//@ variant (x - sum) + 1;
  	//@ invariant sum >= 1;
  	//@ invariant sum > count;
  	//@ invariant count >= 0;

    count++;
    sum = sum + (2*count + 1);
  }
  return count;
}   
