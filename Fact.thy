theory Fact
 
imports Main
 
begin
section \<open>Fixed point semantics\<close>
 
text \<open>
  In this theory, we consider functions as relations, which are sets of pairs
  of elements: a \<R> b is the same as (a, b) \<in> {(x, y). x \<R> y}.
\<close>
text \<open>
  Fac is a functor, which takes a partial function int \<Rightarrow> int
  and yields a more defined partial function.
  Starting from the function defined nowhere (the empty set {}), it
  yields (0,1) which is a partial function that gives the factorial of 0.
  Given a partial function interpreted as a set of (n, n!) pairs, it yields
  a partial function defining the (n+1, (n+1)!) pairs using:
    n! = n (n-1)!, which leads to: (n+1)! = (n+1)n!\<close>

text \<open>
term "{}"
term "Fac {} = {(0,1)}"
term "Fac (Fac {}) = Fac {(0,1)} = {(0,1), (1, 1*1)}"
term "Fac (Fac (Fac {})) = Fac {(0,1), (1,1)} = {(0,1), (1, 1*1), (2, 2*1)}"
term "Fac (Fac (Fac (Fac {}))) = Fac {(0,1), (1,1), (2,2)} = {(0,1), (1, 1*1), (2, 2*1), (3, 3*2)}"
\<close>

definition Fac:: "int rel \<Rightarrow>  int rel"
where
  "Fac f \<equiv> {(0,1)} \<union> image (\<lambda>(n,fac_n). (n+1, (n+1)*fac_n))  f"
 
text \<open>
  Applying Fac ten times to the empty set yields a partial function
  which gives the factorial of 0 .. 9 *)
\<close>
value "(Fac ^^ 10){}"
 
text \<open>Definition of the factorial as the least fixed point of Fac\<close>
definition fac:: "int rel"
where
  "fac \<equiv> lfp Fac"
 
text \<open>
  In order to use this definition, we have to prove that Fac 
  is monotone, i.e. that it always adds information
\<close>
lemma mono_Fac : "mono Fac"
  unfolding mono_def Fac_def by blast


thm funpow_simps_right
text \<open>
  Each time we apply Fac to an approximation of fac, we get a better approximation.
  Show that we can compute 3! using the 4th approximation of the factorial
\<close>
lemma Fac_3 : "(3, 6) \<in> (Fac ^^ Suc(Suc(Suc(Suc 0)))){}"
  by (simp add: Fac_def)
 
text \<open>Show that any iteration of Fac on the empty set is an approximation of fac\<close>
lemma fac_approx : "(Fac ^^ (n::nat)){} \<subseteq> fac"
proof (induction n)
  case 0
  then show ?case by simp
next
  case (Suc n) print_facts
  then show ?case using lfp_unfold[OF mono_Fac] fac_def Kleene_iter_lpfp[OF mono_Fac] by blast
qed
 
thm Set.subsetD
thm Set.subsetD[OF fac_approx]
thm Set.subsetD[OF fac_approx, of _ 4]
 
text \<open>Show that 6 is 3!\<close>
lemma four: "Suc (Suc (Suc (Suc 0))) = 4" by simp
lemma fac_3 : "(3, 6) \<in> fac"
  using Fac_3 fac_approx by blast

end