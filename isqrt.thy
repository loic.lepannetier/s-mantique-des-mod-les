theory isqrt
  imports Main
begin
lemma distr_right: \<open>(x * (y + z)) = ((x * y) + (x * z))\<close>
   sorry

(*lemma sym : \<open>A * B = B * A\<close> by sledgehammer*)

lemma distr_left: \<open>((x + y) * z) = ((x * z) + (y * z))\<close>
  sorry

	(*
	  int isqrt (int x) {
	    //@ requires x > 0 ;
	    //@ ensures ((result * result) <= x) \<and> (x <= ((result + 1) * (result + 1))) ;
	    int count = 0;
	    int sum = 1;
	    while (sum <= x) {
	      //@ variant (x - sum) + 1;
	      //@ invariant sum >= 1;
	      //@ invariant sum > count;
	      //@ invariant count >= 0;
	      count++;  sum = sum + ((2 * count) + 1);
	    }
	    return count;
	  }
	*)

end
